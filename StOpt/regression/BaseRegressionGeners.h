// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef BASEREGRESSIONGENERS_H
#define BASEREGRESSIONGENERS_H
#include "StOpt/regression/BaseRegression.h"
#include <geners/AbsReaderWriter.hh>
#include <geners/associate_serialization_factory.hh>

/** \file BaseRegressionGeners.h
 * \brief Base class mapping with geners to archive BaseRegression pointer type
 * \author  Xavier Warin
 */

/// \¢lass
///  I/O factory for classes derived from .
// Note publication of the base class and absence of public constructors.
class SerializationFactoryForBaseRegression : public gs::DefaultReaderWriter<StOpt::BaseRegression>
{
    typedef DefaultReaderWriter<StOpt::BaseRegression> Base;
    friend class gs::StaticReaderWriter<SerializationFactoryForBaseRegression>;
    SerializationFactoryForBaseRegression();
};

// SerializationFactoryForBaseRegression wrapped into a singleton
typedef gs::StaticReaderWriter<SerializationFactoryForBaseRegression> StaticSerializationFactoryForBaseRegression;

gs_specialize_class_id(StOpt::BaseRegression, 1)
gs_declare_type_external(StOpt::BaseRegression)
gs_associate_serialization_factory(StOpt::BaseRegression, StaticSerializationFactoryForBaseRegression)

#endif
