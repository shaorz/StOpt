// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include "StOpt/regression/ContinuationCuts.h"
#include "StOpt/core/utils/comparisonUtils.h"

using namespace std;
using namespace Eigen;

namespace StOpt
{


ContinuationCuts::ContinuationCuts(const  std::shared_ptr< SpaceGrid >   &p_grid,
                                   const std::shared_ptr< BaseRegression >   &p_condExp,
                                   const ArrayXXd &p_values) : m_grid(p_grid), m_condExp(p_condExp), m_regressedCutCoeff(p_grid->getDimension() + 1)

{
    // nest on cuts
    for (int ic = 0; ic < p_grid->getDimension() + 1; ++ic)
    {
        // coefficients of regressed functions (nb stock points, nb function basis)
        // From Eigen 3.4  ArrayXXd  valLoc = p_values(seqN(ic*p_condExp->getNbSimul(),p_condExp->getNbSimul()),all);
        ArrayXXd  valLoc = p_values.block(ic * p_condExp->getNbSimul(), 0, p_condExp->getNbSimul(), p_values.cols());
        m_regressedCutCoeff(ic) = m_condExp->getCoordBasisFunctionMultiple(valLoc.transpose()).transpose();
    }
    // for first coefficient cuts calculate  \f$ \bar a_0  = a_0 - \sum_{i=1}^d a_i \bar x_i \f$
    // iterator
    shared_ptr<GridIterator> iterRegGrid = m_grid->getGridIterator();
    while (iterRegGrid->isValid())
    {
        // coordinates
        ArrayXd pointCoordReg = iterRegGrid->getCoordinate();
        // point number
        int ipoint =  iterRegGrid->getCount();
        // grid cuts
        for (int id = 0 ; id < pointCoordReg.size(); ++id)
            m_regressedCutCoeff(0).col(ipoint) -= m_regressedCutCoeff(id + 1).col(ipoint) * pointCoordReg(id);
        iterRegGrid->next();
    }
}

ArrayXXd ContinuationCuts::getCutsASim(const ArrayXXd &p_hypStock, const ArrayXd &p_coordinates) const
{
    int nbCutsCoeff = m_grid->getDimension() + 1;
    // for return
    ArrayXXd  cuts(nbCutsCoeff, m_grid->getNbPoints());
    int iPointCut = 0;
    // nest on grid points
    shared_ptr<GridIterator> iterRegGrid = m_grid->getGridIterator();
    while (iterRegGrid->isValid())
    {
        // coordinates
        Eigen::ArrayXd pointCoordReg = iterRegGrid->getCoordinate();
        // test if inside the hypercube
        bool bInside = true;
        for (int id = 0 ; id < pointCoordReg.size(); ++id)
            if (isStrictlyLesser(pointCoordReg(id), p_hypStock(id, 0)) || (isStrictlyMore(pointCoordReg(id), p_hypStock(id, 1))))
            {
                bInside = false;
                break;
            }
        if (bInside)
        {
            // point number
            int ipoint =  iterRegGrid->getCount();

            for (int jc = 0; jc < nbCutsCoeff; ++jc)
            {
                // reconstruct the value for all simulations
                cuts(jc, iPointCut) =  m_condExp->getValue(p_coordinates, m_regressedCutCoeff(jc).col(ipoint));
            }
            iPointCut += 1;
        }
        iterRegGrid->next();
    }
    cuts.conservativeResize(nbCutsCoeff, iPointCut);
    return cuts;
}


ArrayXXd  ContinuationCuts::getCutsAllSimulations(const ArrayXXd &p_hypStock) const
{
    int nbCutsCoeff = m_grid->getDimension() + 1;
    // for return
    ArrayXXd  cuts(nbCutsCoeff * m_condExp->getNbSimul(), m_grid->getNbPoints());
    int iPointCut = 0;
    // nest on grid points
    shared_ptr<GridIterator> iterRegGrid = m_grid->getGridIterator();
    while (iterRegGrid->isValid())
    {
        // coordinates
        Eigen::ArrayXd pointCoordReg = iterRegGrid->getCoordinate();
        // test if inside the hypercube
        bool bInside = true;
        for (int id = 0 ; id < pointCoordReg.size(); ++id)
            if (isStrictlyLesser(pointCoordReg(id), p_hypStock(id, 0)) || (isStrictlyMore(pointCoordReg(id), p_hypStock(id, 1))))
            {
                bInside = false;
                break;
            }
        if (bInside)
        {
            // point number
            int ipoint =  iterRegGrid->getCount();

            for (int jc = 0; jc < nbCutsCoeff; ++jc)
            {
                // reconstruct the value for all simulations
                cuts.col(iPointCut).segment(jc * m_condExp->getNbSimul(), m_condExp->getNbSimul()) = m_condExp->reconstruction(m_regressedCutCoeff(jc).col(ipoint));
            }
            iPointCut += 1;
        }
        iterRegGrid->next();
    }
    cuts.conservativeResize(nbCutsCoeff * m_condExp->getNbSimul(), iPointCut);
    return cuts;
}
}
