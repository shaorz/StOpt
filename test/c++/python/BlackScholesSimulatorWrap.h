// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef BLACKSCHOLESSIMULATORWRAP_H
#define BLACKSCHOLESSIMULATORWRAP_H
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "StOpt/python/Pybind11VectorAndList.h"
#include "test/c++/tools/simulators/BlackScholesSimulator.h"

/** \file BlackScholesSimulatorWrap.h
 *  Defines the wrapping for the Black Scholes Simulator
 * \author Xavier Warin
 */


/// \class BlackScholesSimulatorWrap BlackScholesSimulatorWrap.h
/// Simulator wrap
class BlackScholesSimulatorWrap: public  BlackScholesSimulator
{
public :

    /// \brief Constructor
    /// \param p_initialValues initial values of the assets
    /// \param p_sigma         Volatility vector
    /// \param p_mu            Trend
    /// \param p_correl        Correlation matrix for the assets
    /// \param p_T             Maturity
    /// \param p_nbStep        Number of time step to simulate
    /// \param p_nbSimul       Number of Monte Carlo simulations
    /// \param p_bForward      is it a forward simulator (or a backward one) ?
    BlackScholesSimulatorWrap(const Eigen::VectorXd   &p_initialValues,  const Eigen::VectorXd   &p_sigma,  const Eigen::VectorXd   &p_mu,   const Eigen::MatrixXd &p_correl,
                              const double &p_T, const size_t &p_nbStep, const size_t &p_nbSimul, const bool &p_bForward) :
        BlackScholesSimulator(p_initialValues,   p_sigma,  p_mu,  p_correl,  p_T, p_nbStep, p_nbSimul, p_bForward) {}


    /// \brief get back trend of the asset
    Eigen::VectorXd getMu() const
    {
        Eigen::VectorXd mu = BlackScholesSimulator::getMu();
        return mu ;
    }
};
#endif /*BlackScholesSimulatorWrap.h */
