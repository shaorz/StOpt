// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef DYNAMICPROGRAMMINGBYTREECUT_H
#define DYNAMICPROGRAMMINGBYTREECUT_H
#include <fstream>
#include <memory>
#include <functional>
#include <Eigen/Dense>
#include "StOpt/core/grids/FullGrid.h"
#include "StOpt/dp/OptimizerDPCutTreeBase.h"

/* \file DynamicProgrammingByTreeCut.h
 * \brief Defines a simple  programm  showing how to optimize a problem by dynamic programming where transitional problems are solved using a LP
 *        A simple grid  is used
 *        Uncertainties are defined on a tree.
 * \author Xavier Warin
 */

/// \brief Principal function to optimize  a problemx1
/// \param p_grid              grid used for  deterministic state (stocks for example)
/// \param p_optimize          optimizer defining the optimisation between two time steps
/// \param p_funcFinalValue    function defining the final value with cuts
/// \param p_pointStock        point stock used for interpolation at initial date
/// \param p_initialRegime     regime at initial date
/// \param p_fileToDump        file to dump continuation values
///
double  DynamicProgrammingByTreeCut(const std::shared_ptr<StOpt::FullGrid> &p_grid,
                                    const std::shared_ptr<StOpt::OptimizerDPCutTreeBase > &p_optimize,
                                    const std::function< Eigen::ArrayXd(const int &, const Eigen::ArrayXd &, const Eigen::ArrayXd &)>   &p_funcFinalValue,
                                    const Eigen::ArrayXd &p_pointStock,
                                    const int &p_initialRegime,
                                    const std::string   &p_fileToDump
                                   );

#endif /* DYNAMICPROGRAMMINGBYTREECUT_H */
