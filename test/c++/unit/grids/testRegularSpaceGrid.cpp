#define BOOST_TEST_MODULE StOptRegularSpaceGrid

#include <iomanip>
#include <boost/test/unit_test.hpp>
#include <boost/log/trivial.hpp>
#include <boost/format.hpp>
using boost::format;

#include "StOpt/core/grids/RegularSpaceGrid.h"
#include "StOpt/core/utils/constant.h"

BOOST_AUTO_TEST_CASE(StOptRegularSpaceGrid)
{
    size_t nsteps = 1000;
    double step = 400.0 / (1.0 * nsteps);
    Eigen::ArrayXd minimum(1);
    for (size_t iPoint = 0; iPoint <= nsteps; iPoint++)
    {
        minimum(0) = iPoint * step;
        if (minimum(0) > 135.0 && minimum(0) < 135.3)
            break;
    }
    //minimum(0) \approx 135.20000000000002;

    Eigen::ArrayXd step_grid(1);
    step_grid(0) = (140. - minimum(0));
    Eigen::ArrayXi nsteps_grid(1);
    nsteps_grid(0) = 1;
    StOpt::RegularSpaceGrid grid = StOpt::RegularSpaceGrid(minimum, step_grid, nsteps_grid);

    Eigen::ArrayXd point(1);
    point(0) = 135.19999999999999;

    std::stringstream ss_minimum;
    ss_minimum << std::fixed << std::setprecision(15) << minimum(0);
    BOOST_LOG_TRIVIAL(info) << "minimum: " << ss_minimum.str();
    std::stringstream ss_point;
    ss_point << std::fixed << std::setprecision(15) << point(0);
    BOOST_LOG_TRIVIAL(info) << "point: " << ss_point.str();
    BOOST_LOG_TRIVIAL(info) << "grid.isInside(point): " << grid.isInside(point);
    if (grid.isInside(point))
    {
        Eigen::ArrayXi result = grid.lowerPositionCoord(point); // Assertion fails.
        BOOST_LOG_TRIVIAL(info) << "grid.lowerPositionCoord(point): " << result;
    }
}
