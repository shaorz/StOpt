# find StOpt library 
# This module finds StOpt if it is installed and determines where the include files are.
# It sets the following variables
# STOPT_FOUND          - have the STOPT LIBS been found
# STOPT_LIBRARIES      - path to StOpt libraries
# STOPT_INCLUDE_DIRS   - path where to find Stopt include

# module directory
set( STOPT_ROOT STOPT_ROOT-NOT-FOUND CACHE PATH
  "Dependance StPOt. Please set STOPT_ROOT variables.")
if (NOT STOPT_ROOT)
  message(FATAL_ERROR
    "StOpt directory not found. Please set the variable 'STOPT_ROOT'.")
endif(NOT STOPT_ROOT)

if (MSVC AND (CMAKE_GENERATOR MATCHES "^Visual.*Win64$"))
    set (BUILD build/x64/)
else (MSVC AND (CMAKE_GENERATOR MATCHES "^Visual.*Win64$"))
    set (BUILD build/)
endif (MSVC AND (CMAKE_GENERATOR MATCHES "^Visual.*Win64$"))

FIND_LIBRARY(STOPT_LIBRARY
  NAMES StOpt
  PATHS
  ${STOPT_LIBRARIES}
  ${STOPT_ROOT}/${BUILD}
  ${STOPT_ROOT}/${BUILD}/Release
  ${STOPT_ROOT}/${BUILD}/RelWithDebInfo
  ${STOPT_ROOT}/lib
  )


FIND_PATH(STOPT_INCLUDE_DIR
  NAMES /StOpt/core/utils/version.h
  PATHS
    ${STOPT_ROOT}
    ${STOPT_ROOT}/include
    ${STOPT_INCLUDE_DIR}
    ${STOPT_INCLUDE_DIRS}
    )


MARK_AS_ADVANCED(
  STOPT_LIBRARY
  STOPT_INCLUDE_DIR
  )

SET(STOPT_INCLUDE_DIRS "${STOPT_INCLUDE_DIR}")
SET(STOPT_LIBRARIES "${STOPT_LIBRARY}")
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(StOpt  DEFAULT_MSG  STOPT_LIBRARIES STOPT_INCLUDE_DIRS)
